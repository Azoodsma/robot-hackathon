#include <Arduino.h>
// defs for ultrasound-echo
#define echoPin 4 // Echo Pin
#define trigPin 5 // Trigger Pin
#define LEDPin 13 // Onboard LED
//range for ultrasound
int maximumRange = 200; // Maximum range needed
int minimumRange = 0; // Minimum range needed
long duration, distance; //duration used to calculate distance
/*
    L = Left
    R = Right
    F = forward
    B = backward
*/
// pins for the servo
#include <Servo.h>
int pinLB = 12;      // define pin 12
int pinLF = 3;       // define pin 3
int pinRB = 13;      // define pin 13
int pinRF = 11;      // define pin 11
// pins for sensor
int inputPin = 4;    // define pin for sensor echo
int outputPin =5;    // define pin for sensor trig
// the speed for servo
int Fspeedd = 2;      // forward speed
int Rspeedd = 0;      // right speed
int Lspeedd = 0;      // left speed
//direction
int directionn = 0;   // forward=8 backward=2 left=4 right=6
Servo myservo;        // set myservo
int delay_time = 250; // settling time after steering servo motor moving B
int Fgo = 8;         // Move F
int Rgo = 6;         // move to the R
int Lgo = 4;         // move to the L
int Bgo = 2;         // move B



void setup() {
 Serial.begin (9600);
 // setup for sensor
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 pinMode(LEDPin, OUTPUT); // Use LED indicator (if required)
// setupo for servo
 pinMode(pinLB,OUTPUT); // pin 12
 pinMode(pinLF,OUTPUT); // pin 3 (PWM)
 pinMode(pinRB,OUTPUT); // pin 13
 pinMode(pinRF,OUTPUT); // pin 11 (PWM)
 pinMode(inputPin, INPUT);    // define input pin for sensor
 pinMode(outputPin, OUTPUT);  // define output pin for sensor
 myservo.attach(9);    // Define servo motor output pin to D9 (PWM)
}

void loop (){
    myservo.write(90);
    //loop to calculate distance object
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);

    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);

    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);

    //Calculate the distance (in cm) based on the speed of sound.
    distance = duration/58.2;

    digitalWrite(pinLB,HIGH);  // motor moves to left rear
    digitalWrite(pinRB,HIGH);  // motor moves to right rear
    analogWrite(pinLF,255);
    analogWrite(pinRF,255);
    /*
    digitalWrite(pinLB,LOW);   // right wheel moves forward
    digitalWrite(pinRB, LOW);  // left wheel moves forward
    analogWrite(pinLF,255);
    analogWrite(pinRF,255);*/
}
